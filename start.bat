:: NASA-TLX Web form startup script for Windows based systems
@echo off

echo --NASA TLX Web form startup script
echo --version 2019-04-29
echo ----------

SET /p port="Enter server port (Default 8080): "
IF EXIST server.js ( GOTO SERVER-FILE-EXISTS ) ELSE ( GOTO SERVER-FILE-NOT-EXISTS )

:SERVER-FILE-EXISTS
echo --Server file ok

IF EXIST node_modules\ ( GOTO SERVER-MODULES-EXISTS ) ELSE ( GOTO SERVER-MODULES-NOT-EXISTS )

:SERVER-FILE-NOT-EXISTS
echo --Server files are missing. Please run this script at server files root folder
pause
exit

:SERVER-MODULES-EXISTS
echo --Server modules ok, checking for module updates
call npm install
echo --Starting up server, use CTRL-C to exit
call node server.js -p %port%
pause
exit

:SERVER-MODULES-NOT-EXISTS
echo --Server modules missing, fetching server modules from internet
call npm install
echo --Starting up server, use CTRL-C to exit
call node server.js -p %port%
pause
exit
