# NASA-TLX -verkkolomake

Nasa-TLX -lomake verkkoselaimille.

## Palvelimen käyttöönotto

Node.js sekä node package manager (npm) vaaditaan asennuksen onnistumiseksi. Käynnistääksesi palvelinsovelluksen, suorita start.bat (Windows-järjestelmällä) tai start.sh (Linux-pohjaisella järjestelmällä). Käynnistysskripti asentaa tarpeelliset moduulit ensimmäisellä käyttökerralla. Skripti kysyy myös portin, jossa palvelin pyörii, oletusarvona käytetään porttia 8080.

Palvelimen voi käynnistää myös manuaalisesti:
```
$ npm install
$ node server.js [-p PORTTI]
```

Seuraavanlaisen tulosteen pitäisi ilmaantua, kunhan palvelin on käynnissä:
```
TLX Form Server is now running on <VALITSEMASI PORTTI tai oletusarvo 8080>.
Connected to disk-based main database file.
```
Kirjaudu sisään verkkosovellukseen osoitteessa http://localhost:PORTTI kunhan sovellus on käynnissä. Vakiotunnukset ovat "admin" ja "testPassword". Salasana kannattaa vaihtaa kirjautumisen jälkeen. Tämä onnistuu ylläpitonäkymästä. Palvelimen ja ylläpitäjän käyttäjätunnusten alustamisen jälkeen voit hallinnoida lomakkeita sekä ladata vastauksia verkkokäyttöliittymästä.

## Lomakkeiden hallinnoiminen (lisääminen, muokkaaminen ja poistaminen)
Voit hallinnoida lomakkeita ylläpitonäkymästä (http://localhost:PORTTI/admin) kirjauduttuasi. Vanhentuneisiin lomakkeisiin ei voi vastata. Vanhentumisaika tarkistetaan palvelintietokoneen ajasta: jos palvelinkoneen aika on vanhentumisajan jälkeinen, lomakkeen linkki ei enää toimi. Lomake vastauksineen pysyy kuitenkin järjestelmässä, ja voit ladata sen vastaukset ylläpitonäkymästä, tai esimerkiksi tarvittaessa antaa lisäaikaa vastaamiselle lomakkeen jo kerran vanhennuttua.

## Vastausten lataaminen
Ladataksesi vastaukset luotuun lomakkeeseen, etsi lomake ylläpitonäkymän listauksesta ja valitse "Lataa .csv" -linkki. Jos vastauksia ei ole tullut, linkkiä ei näytetä. Vastausten lukumäärä näkyy myös listauksessa.

## Vianmääritys

### Kadonnut/unohtunut salasana
Jos se on tarpeen, allaolevalla menetelmällä voit palauttaa salasanan palvelintietokoneella.
1. Asenna DB Browser for SQLite tai vastaava ohjelma tietokannan muokkaamiseen
2. Avaa db/formData.db tietokantaselaimella
3. User-taulussa lisää seuraava tiivistearvo password-kenttään. Tämä tiivistearvo vastaa vakiosalasanaa "testPassword".
```
$2b$10$.3TGy2vw9ZwgLluLqlG8YOux8vXIxR3qS49loRgSRl.k08cmqrni.
```
4. Tallenna/kirjoita muutokset tietokantaan.
5. Nyt pitäisi olla mahdollista kirjautua käyttäen vakiosalasanaa "testPassword".

Vakiona järjestelmässä on vain yksi ylläpitotili.

### Ongelmia vastausten lataamisen kanssa
- Varmista, että kansio nimeltä xlsx on olemassa tämän kansion sisällä. Tavallisesti kyseinen kansio luodaan automaattisesti.

### Muuta/yleistä
- Sovelluksen uudelleenkäynnistäminen saattaa auttaa joissakin tilanteissa