// NASA TLX Web form Admin panel

var supportedLanguages = ['en', 'fi'];
var currentLanguage;

var cachedFormData = undefined;

var currentAccount = undefined;

const urlParams = new URLSearchParams(window.location.search);

// type = 0     Normal admin panel
// type = 1     Modify form panel
function initializeAdminPanel(type) {
    let langParam = urlParams.get('lang');
    if (langParam !== null && supportedLanguages.includes(langParam)) {
        changeLanguage(langParam);   
    }
    else {
        detectLanguage();
    }

    if (type === 0) {
        console.log("type 0");
        $.get("/admin/getAccount", (data) => {

        }).done((data) => {
            if (data.nextStep === "accountData") {
                currentAccount = data.additionalData;
                insertLoginStatus();
            }
            else {
                window.location.replace("../index.html");
            }
        }).fail((err) => {
            console.log(err);
            currentAccount = "Unknown user";
        });
    }
    else if (type === 1) {
        $("#nameInput").val(urlParams.get('name'));
        $("#taskInput-en").val(urlParams.get('task_en'));
        $("#taskInput-fi").val(urlParams.get('task_fi'));
        $("#expireDate").val(urlParams.get('date'));
        $("#expireTime").val(urlParams.get('time'));
    }

    let submitButton;
    let postLocation;
    if (type === 0) {
        submitButton = "#submitNewForm";
        postLocation = '/form/add';

        // Change password start
        $("#submitPass").click(function (event) {
            event.preventDefault();
            $.post("/admin/changePass", $("#changePass").serializeArray(), (data) => {

            }).done((data) => {
                if (data.nextStep === "success") {
                    if (currentLanguage === "fi") {
                        alert("Salasana vaihdettiin onnistuneesti.");
                    }
                    else {
                        alert("Password change successful.");
                    }
                }
                else if (data.nextStep === "passwordNotAcceptable") {
                    if (currentLanguage === "fi") {
                        if (data.additionalData.isLongEnough) {
                            alert("Salasana ei kelpaa. Varmista, että molemmissa kentissä on sama salasana.");
                        } else {
                            alert("Salasana ei kelpaa. Varmista, että salasanasi on yli 6 merkkiä pitkä ja sama molemmissa kentissä.");
                        }
                    }
                    else {
                        if (data.additionalData.isLongEnough) {
                            alert("Password not accepted. Please ensure that both fields have the same password.");
                        } else {
                            alert("Password not accepted. Please ensure that your password is over 6 characters long and the same in both fields.");
                        }                    
                    }
                }
                else if (data.nextStep === "couldNotHashPassword") {
                    if (currentLanguage === "fi") {
                        alert("Virhe luodessa salasanan tiivistearvoa. Yritä uudelleen ja ota tarvittaessa yhteys ylläpitoon.");
                    }
                    else {
                        alert("Error creating a hash value for the password. Please try again later and contact the administrators if necessary.");
                    }
                }
                else {
                    if (currentLanguage === "fi") {
                        alert("Virhe muutettaessa salasanaa.");
                    }
                    else {
                        alert("Error changing password.");
                    }
                }
            }).fail((err) => {
                console.log(err.message);
                alert("Error occurred: ", err.message);
            });
        });
        // change password end
    }
    else if (type === 1) {
        submitButton = "#submitModifyForm";
        postLocation = '/form/modify'
        $("#formId").val(urlParams.get('id'));
    }

    $(submitButton).click((event) => {
        event.preventDefault();
        let id = undefined;
        if (type === 1) {
            id = $("#formId").val();
        }
        let dateValue = $("#expireDate").val();
        let timeValue = $("#expireTime").val();
        let nameValue = $("#nameInput").val();
        let taskValue_en = $("#taskInput-en").val();
        let taskValue_fi = $("#taskInput-fi").val();

        let nullCheck = dateValue != null && timeValue != null && nameValue != null && taskValue_en != null && taskValue_fi != null;
        let lengthCheck = dateValue.length > 0 && timeValue.length > 0 && nameValue.length > 0 && taskValue_en.length > 0 && taskValue_fi.length > 0;

        if (nullCheck && lengthCheck) {
            // do it
            let json = {
                task : {
                    en : taskValue_en,
                    fi : taskValue_fi 
                }
                ,
                name : {
                    value : nameValue
                }
                ,
                date : {
                    value : dateValue + "_" + timeValue
                }  
            };

            //alert("OK values, proceed.");
            //alert(JSON.stringify(json.task));
            
            $.post(postLocation, { name : json.name.value, task : JSON.stringify(json.task), expireDate : json.date.value, id : id}, data => {

            }).done(data => {
                if (type === 0) {
                    if (!data.actionValid) {
                        alert(data.nextStep);
                    }
                    else {
                        if (currentLanguage === 'fi') {
                            alert("Luotiin onnistuneesti.");
                            
                        }
                        else {
                            alert("Created successfully.");
                        }
                        window.location.reload();
                    }
                }
                else if (type === 1) {
                    if (currentLanguage === 'fi') {
                        if (data.nextStep === "notLoggedIn") {
                            alert("Kirjaudu sisään, sinulla ei ole oikeuksia.");
                        }
                        else if (data.nextStep === "formModifySuccess") {
                            alert("Muokkaus onnistui, paina OK palataksesi ylläpitonäkymään");
                            window.location.replace("/admin?lang=" + currentLanguage);
                        }
                        else {
                            alert("Muu virhe");
                        }
                    }
                    else {
                        if (data.nextStep === "notLoggedIn") {
                            alert("Log in, you do not have the permissions necessary.");
                        }
                        else if (data.nextStep === "formModifySuccess") {
                            alert("Editing successful, press OK to return to the admin panel.");
                            window.location.replace("/admin?lang=" + currentLanguage);
                        }
                        else {
                            alert("Unspecified error");
                        }
                    }
                }
            });
        }
        else {
            if (currentLanguage === 'fi') {
                alert("Täytä kaikki lomakkeen kentät sopivilla arvoilla.");
            }
            else {
                alert("Please fill the whole form with valid values.");
            }
        }
    });

    if (type === 0) {
        getForms();
    }
}

// log out from site
function logOut() {
    $.get("/logOut", (data) => {

    }).done((data) => {
        if (data.nextStep === "goToFrontPage") {
            window.location.replace("/index.html?lang=" + currentLanguage);
        }
        else {
            alert("Virhe/Error: ", data.additionalData);
        }
    }).fail((err) => {
        alert("Virhe/Error: ", err);
    });
}

// get form data
function getForms() {
    $.get("/forms", data => {

    }).done(data => {
        console.log(data);
        if (data.auth && data.nextStep === "listForms") {
            cachedFormData = data.additionalData;
            generateTable(data.additionalData, "forms");
        }
        else {
            cachedFormData = undefined;
            document.getElementById("forms").innerHTML = '<p lang="fi">Et ole kirjautuneena sisään</p><p lang="en">You are not logged in</p>';
        }
        
    }).fail(err => {
        console.log("error fetching form data");
    });
}

// move to modify page
function modifyForm() {
    let id = undefined;
    $("input[name=selection]").each(function() {
        if (this.checked) {
            id = this.value;
        }
    });
   console.log(id);

   if (id !== undefined && !isNaN(id)) {
        let d;
        for (let i = 0 ; i < cachedFormData.length; ++i) {

            if (id == cachedFormData[i]['id']) {
                d = i;
                break;
            }
        }
        let name = cachedFormData[d]['name'];
        let tasks = cachedFormData[d]['task'];
        let date = cachedFormData[d]['expireDate'];
        let datevalue = null;
        let timevalue = null;
        if (date !== null) {
            datevalue = date.split("_", 2)[0];
            timevalue = date.split("_", 2)[1];
        }

        let tasksjson = null;
        let task_fi = null;
        let task_en = null;
        if (tasks !== null) {
            tasksjson = JSON.parse(tasks);
            task_fi = tasksjson.fi;
            task_en = tasksjson.en;
        }

        if (name === null || datevalue === null || timevalue === null || tasksjson === null || task_fi === null || task_en === null) {
            window.location.assign("/admin/modifyForm.html?id=" + id + "&lang=" + currentLanguage);
        }
        else {
            window.location.assign("/admin/modifyForm.html?id=" + id + "&lang=" + currentLanguage + "&name=" + name + "&task_fi=" + task_fi + "&task_en=" + task_en + "&date=" + datevalue + "&time=" + timevalue);
        }
        
   }
   else {
        if (currentLanguage === 'fi') {
            alert("Valitse lomake ennen toiminnon aloittamista.");
        }
        else {
            alert("Please select a form before attempting to perform an action.");
        }
    }
}

// remove form
function removeForm() {
    let id = undefined;
    $("input[name=selection]").each(function() {
        if (this.checked) {
            id = this.value;
        }
    });
   console.log(id);

   let question = "Are you sure that you want to delete the chosen form?";

   if (currentLanguage === "fi") {
       question = "Oletko aivan varma, että haluat poistaa valitun kyselyn?";
   }

   if (confirm(question)) {
        if (id !== undefined && !isNaN(id)) {
            $.post('/form/remove', { id : id }, data => {

            }).done(data => {
                if (data.actionValid && data.nextStep === "formRemovalSuccess") {
                    if (currentLanguage === 'fi') {
                        alert("Lomakkeen poistaminen onnistui.");
                    }
                    else {
                        alert("Form removal successful.");
                    }
                    window.location.reload();
                }
            }).fail(err => {
                if (currentLanguage === 'fi') {
                    alert("Virhe poistaessa lomaketta.");
                }
                else {
                    alert("Error occurred while removing a form.");
                }
            });
    }
    else {
            if (currentLanguage === 'fi') {
                alert("Valitse lomake ennen toiminnon aloittamista.");
            }
            else {
                alert("Please select a form before attempting to perform an action.");
            }
    }
   }
}

// generate html table to divider from given data
function generateTable(data, targetDiv) {
    let container = document.getElementById(targetDiv);

    if (data !== null && container !== null) {
        var columns = [];

        // Push Selection column
        columns.push("SEL");

        // Columns
        for (var i = 0; i < data.length; i++) {
            for (var key in data[i]) {
                if (columns.indexOf(key) === -1) {
                    columns.push(key);
                }
            }
        }

        // Push csv column
        columns.push("CSV");

        // Push link column
        columns.push("link");
        
        // Create table
        var table = document.createElement("table");

        // Create row
        var table_tr = table.insertRow();

        // Add columns
        for (var i = 0; i < columns.length; i++) {
            var table_th = document.createElement("th");
            var columnText = columns[i];
            if (currentLanguage === 'fi') {
                if (columnText === 'name') {
                    columnText = 'Nimi';
                }
                else if (columnText === 'task') {
                    columnText = 'Tehtävä';
                }
                else if (columnText === 'expireDate') {
                    columnText = 'Vanhentumispäivämäärä';
                }
                else if (columnText === 'SEL') {
                    columnText = 'Valinta';
                }
                else if (columnText === 'responseAmount') {
                    columnText = 'Vastausten lukumäärä';
                }
                else if (columnText === "link") {
                    columnText = "Linkki"
                }
            }
            else {
                if (columnText === 'name') {
                    columnText = 'Name';
                }
                else if (columnText === 'task') {
                    columnText = 'Task';
                }
                else if (columnText === 'expireDate') {
                    columnText = 'Expiration date';
                }
                else if (columnText === 'SEL') {
                    columnText = 'Selection';
                }
                else if (columnText === 'responseAmount') {
                    columnText = 'Amount of responses';
                }
                else if (columnText === "link") {
                    columnText = "Link"
                }
            }
            table_th.innerHTML = columnText;
            table_tr.appendChild(table_th);
        }

        // Add data on row per row basis
        for (var i = 0; i < data.length; i++) {
            table_tr = table.insertRow();

            for (var j = 0; j < columns.length; j++) {
                var cell = table_tr.insertCell();
                var cellData = data[i][columns[j]];
                if (columns[j] === "task") {
                    var json = JSON.parse(cellData);
                    if (json !== null && json.en !== null && json.fi !== null) {
                        cellData = '<p>' + json.en + '</p>' + '<p>' + json.fi + '</p>';
                    }
                    else {
                        cellData = '~null~';
                    }
                }
                else if (columns[j] === "CSV") {
                    if (data[i]["responseAmount"] == 0) {
                        if (currentLanguage === 'fi') {
                            cellData = 'Ei ladattavia vastauksia.';
                        }
                        else {
                            cellData = 'No responses to download.';
                        }
                    } else {
                        if (currentLanguage === 'fi') {
                            cellData = '<a href="/data/process?id=' + data[i]['id'] + '">Lataa</a>';
                        }
                        else {
                            cellData = '<a href="/data/process?id=' + data[i]['id'] + '">Download</a>';
                        }
                    }   
                }
                else if (columns[j] === "SEL") {
                    cellData = '<input type="radio" name="selection" value="' + data[i]['id'] + '"/>';
 
                }
                else if (columns[j] === "expireDate") {
                    cellData = cellData.replace("_", " ");
                } else if (columns[j] === "link") {
                    if (currentLanguage === 'fi') {
                        cellData = '<a href="/form.html?formID=' + data[i]['id'] + '">Linkki</a>';
                    }
                    else {
                        cellData = '<a href="/form.html?formID=' + data[i]['id'] + '">Link</a>';
                    }
                }
                cell.innerHTML = cellData;
            }
        }

        // Finally, add table to the target divider
        container.innerHTML = "";
        container.appendChild(table);
    }
    else {
        console.log("Error: Invalid target divider or data given. The page will be missing table(s).");
    }
}

// changes the language to Finnish or English (default) based on the browser's language
function detectLanguage() {
    var browserLang = window.navigator.language;

    if(browserLang.substr(0, 2) == "fi") {
        changeLanguage("fi");
    } else {
        changeLanguage("en");
    }
}

// changes language to one of the supported languages based on the lang parameter
function changeLanguage(lang) {
    if (currentLanguage !== lang && supportedLanguages.includes(lang)) {
        // hide all elements that do not have the specified lang attribute
        document.querySelectorAll("[lang]:not(:lang(" + lang + "))").forEach(function (element) {
            element.style.display = "none";
        });
        
        // display all elements that have the specified lang attribute
        document.querySelectorAll("[lang]:lang(" + lang + ")").forEach(function (element) {
            element.style.display = "block";
        });
        
        elementStrings.forEach((element) => {
            let feElem = document.getElementById(element.elementId);

            if (feElem !== null) {
                feElem.setAttribute(element.attribute, element.strings[lang]);
            }
        });

        currentLanguage = lang;

        if (cachedFormData !== undefined) {
            generateTable(cachedFormData, "forms");
        }

        if (currentAccount !== undefined) {
            insertLoginStatus();
        }
    }
}

// insert login status
function insertLoginStatus() {
    if (currentLanguage === 'fi') {
        document.getElementById("loginStatus").innerText = "Olet kirjautuneena sisään käyttäjänä " + currentAccount;
    }
    else {
        document.getElementById("loginStatus").innerText = "You're logged in as " + currentAccount;
    }
}

// localization strings for elements, that do not function well with html lang
var elementStrings = [
{
    elementId : "nameInput",
    attribute : "placeholder",
    strings: {
        en : "Form name (only visible for admins)",
        fi : "Kyselyn nimi (vain näkyvillä ylläpitäjille)"
    }
},
{
    elementId : "taskInput-en",
    attribute : "placeholder",
    strings: {
        en : "Form task in english",
        fi : "Kyselyn tehtävä englanniksi"
    }
},
{
    elementId : "taskInput-fi",
    attribute : "placeholder",
    strings: {
        en : "Form task in finnish",
        fi : "Kyselyn tehtävä suomeksi"
    }
},
{
    elementId : "submitNewForm",
    attribute : "value",
    strings: {
        en : "Create new form",
        fi : "Luo uusi kysely"
    }
},
{
    elementId : "submitModifyForm",
    attribute : "value",
    strings: {
        en : "Confirm edit",
        fi : "Varmista muokkaus"
    }
},
{
    elementId : "pass",
    attribute : "placeholder",
    strings: {
        en : "New password",
        fi : "Uusi salasana"
    }
},
{
    elementId : "passAgain",
    attribute : "placeholder",
    strings: {
        en : "New password again",
        fi : "Uusi salasana uudestaan"
    }
},
{
    elementId : "submitPass",
    attribute : "value",
    strings: {
        en : "Change password",
        fi : "Vaihda salasana"
    }
}
];