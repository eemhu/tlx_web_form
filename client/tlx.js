var supportedLanguages = ["fi", "en"];
var currentLanguage;
var mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration;
const urlParams = new URLSearchParams(window.location.search);
var formID;
var tasks;

// initializes the form after loading the page
// creates the TLX scales, detects the language, validates the formID and displays the task in the form
function initializeForm() {
    createScales();

    let langParam = urlParams.get('lang');
    if (langParam !== null && supportedLanguages.includes(langParam)) {
        changeLanguage(langParam);   
    }
    else {
        detectLanguage();
    }

    formID = urlParams.get("formID");
    $.get("/form", {formID: formID}).done(function (data) {
        if (data.nextStep === "formExpired") {
            if (currentLanguage === "fi") {
                window.location.replace("/formExpired.html?lang=fi");
            }
            else {
                window.location.replace("/formExpired.html?lang=en");
            }   
        }
        else if (data.nextStep !== "goToForm") {
            // TODO add better error handling (redirect to front page?)
            if (currentLanguage === "fi") {
                //alert("VIRHE: Hakemaasi lomaketta ei löytynyt! Ethän täytä lomaketta; sitä ei voida valitettavasti käsitellä.");
                window.location.replace("/invalidFormId.html?lang=fi");
            } else {
                //alert("ERROR: Could not find the form! Please do not fill the form, as it cannot be processed.")
                window.location.replace("/invalidFormId.html?lang=en");
            }
        } else {
            try {
                tasks = JSON.parse(data.additionalData["task"]);
                localizeTask();
            } catch(err) {
                // TODO better error handling, should not occur
                console.log("Error parsing tasks");
            }
        }
    });
}

// adds the TLX scales and a submit button to the form
function createScales() {
    document.querySelectorAll(".scaleDiv").forEach(function (element) {
        if (element.innerHTML == "") {
            var html = "<table class=scaleTable><tr>";

            for(var i = 1; i <= 20; i++) {
                html += '<td class="scaleCell" id="' + element.id + '-' + i + '" onClick="cellSelected(\'' + element.id + '-' + i + '\');"></td>';

                // add a vertical line in the middle
                if (i === 10) {
                    html += '<td class="midpointLine"></td>';
                }
            }

            html += "</tr></table>";

            element.innerHTML = html;
        }
    });

    document.getElementById("scaleContainerDiv").innerHTML += '<br><input type="button" id="submitForm" value="OK&rarr;" onclick="submitScales();" disabled/>';
}

// called when a cell (td) is selected on a scale
// updates the corresponding variable and colors the selected cell
function cellSelected(cellId) {
    var scale = cellId.split("-")[0];
    var value = cellId.split("-")[1];

    switch(scale) {
        case "mentalDemand":
            console.log("mentalDemand " + value);
            mentalDemand = value;
            break;
        case "physicalDemand":
            console.log("physicalDemand " + value);
            physicalDemand = value;
            break;
        case "temporalDemand":
            console.log("temporalDemand " + value);
            temporalDemand = value;
            break;
        case "performance":
            console.log("performance " + value);
            performance = value;
            break;
        case "effort":
            console.log("effort " + value);
            effort = value;
            break;
        case "frustration":
            console.log("frustration " + value);
            frustration = value;
            break;
        default:
            console.log("Something went wrong.");
            break;
    } 

    // color the selected cell and make other cells on the same scale look normal
    document.querySelectorAll(".scaleCell").forEach(function (element) {
        var cellScale = element.id.split("-")[0];

        if (element.id === cellId) {
            element.classList.add("selectedCell");
        } else if (cellScale === scale) {
            element.classList.remove("selectedCell");
        }
    });

    checkForm(0); // check form on cell selection
    
}

// checks that form is filled before submission
// type = 0 "DISABLE/ENABLE BUTTON"
// type = 1/other "RETURN TRUE/FALSE"
function checkForm(type) {
    var testSuccess = document.getElementById("nameInput").value.length >= 1 && mentalDemand != null && physicalDemand != null && temporalDemand != null && performance != null && effort != null && frustration != null && formID != null;
    console.log("checkForm: " + testSuccess);
    if (type === 0) {
        if (testSuccess) {
            document.getElementById("submitForm").disabled = false;
        }
        else {
            document.getElementById("submitForm").disabled = true;
        }
    }
    else {
        return testSuccess;
    }
}


// changes the language to Finnish or English (default) based on the browser's language
function detectLanguage() {
    var browserLang = window.navigator.language;

    if(browserLang.substr(0, 2) == "fi") {
        changeLanguage("fi");
    } else {
        changeLanguage("en");
    }
}

// changes language to one of the supported languages based on the lang parameter
function changeLanguage(lang) {
    if (supportedLanguages.includes(lang)) {
        // hide all elements that do not have the specified lang attribute
        document.querySelectorAll("[lang]:not(:lang(" + lang + "))").forEach(function (element) {
            element.style.display = "none";
        });
        
        // display all elements that have the specified lang attribute
        document.querySelectorAll("[lang]:lang(" + lang + ")").forEach(function (element) {
            element.style.display = "block";
        });
        
        currentLanguage = lang;
        localizeTask();
    }
}

// updates the displayed task based on the current language (if the tasks have been retrieved)
function localizeTask() {
    if (tasks !== undefined) {
        if (currentLanguage === "fi") {
            document.getElementById("taskInput").value = tasks["fi"];
        } else {
            document.getElementById("taskInput").value = tasks["en"];
        }
    }
}

// called when selecting the submit button, sends form data to server
function submitScales() {
    var name = document.getElementsByName("name")[0].value;
    var task = document.getElementsByName("task")[0].value;

    if (checkForm(1)) {
        $.post( "/form/submit", { name: name, task: task, formID: formID, mentalDemand: mentalDemand, physicalDemand: physicalDemand, temporalDemand: temporalDemand, performance: performance, effort: effort, frustration: frustration }, function (data) {
            if (data.nextStep === "submissionSuccess") {
                if (currentLanguage === "fi") {
                    //document.getElementById("tlxForm").innerHTML = "Kiitos! Tulokset on tallennettu.";
                    window.location.replace("/submitok.html?lang=fi");
                } else {
                    //document.getElementById("tlxForm").innerHTML = "Thank you! The results have been saved.";
                    window.location.replace("/submitok.html?lang=en");
                }
            } else if (data.nextStep === "errorProcessingSubmission") {
                // TODO add better error handling and validation
                if (currentLanguage === "fi") {
                    alert("Jotain meni vikaan! Varmista, että täytit koko lomakkeen ja yritä uudelleen. Tarvittaessa ota yhteyttä kyselyn järjestäjiin.");
                } else {
                    alert("Something went wrong! Please ensure that you filled the entire form and try again. If necessary, contact the organizers.")
                }
            }
        });
    }
    else {
        if (currentLanguage === "fi") {
            alert("Täytäthän kaikki lomakkeen osat. Jos olet täyttänyt ne jo, yritä uudelleen.");
        }
        else {
            alert("Please fill in the whole form. If you have already done so, please try again.");
        }
    }
}