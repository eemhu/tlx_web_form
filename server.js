/* TLX Web Form Server
    created on 8.3.2019
*/

// Port used for communication
var port = 8080;

// Read command-line arguments
process.argv.forEach(function (val, index, array) {
    if (index === 0 || index === 1) {
        /* Ignore node path and script path */
    }
    else if ((val === "--port" || val === "-p") && !isNaN(array[index + 1])) {
        port = array[index + 1];
        console.log("[Startup] Received argument: " + val + " " + array[index + 1]);
        if (port < 0 || port >= 65536) {
            console.log("Invalid port set. Port must be between 0 and 65536.");
            console.log("Program will now close with exit code 1.");
            process.exit(1);
        }
    }
});

// Import filesystem support
const fs = require('fs');

// xlsx folder check & initialization
try {
    if (!fs.existsSync("xlsx")) {
        fs.mkdirSync("xlsx");
        console.log("Xlsx folder doesn't exists - Created it now");
    }
    else {
        console.log("Xlsx folder exists - OK");
    }
}
catch (xlsxCreationError) {
    console.log("Fatal error: Reading xlsx folder failed - csv downloads may not work. Please shut down the server and create the folder manually or try again.");
    console.log("Additional information: %s", xlsxCreationError.message);
}

// Import moment support
const moment = require('moment');
// Import xlsx support (excel)
const xlsx = require('xlsx');
// Import SQLite3 support
const sqlite3 = require('sqlite3').verbose();
// Import Express support
const express = require('express');
const app = express();
// Import and use CORS middleware w/ express
let cors = require('cors');
app.use(cors());
// Use bodyParser w/ express
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Use express-session for login tracking
let session = require('express-session');
let SQLiteStore = require('connect-sqlite3')(session);
let methodOverride = require('method-override');
let cookieParser = require('cookie-parser');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
    store: new SQLiteStore({
        db: 'sessions.db',
        dir: './db/',
        concurrentDB: true
    }),
    secret: 'p2m7nEmncSBjvrDZC7UYNs9cdzFQiG8v',
    cookie: { maxAge: 60 * 60 * 1000 }, // 1 hour
    resave: true,
    saveUninitialized: false
}));

// Check authorization before serving admin pages
app.all('/admin/*', (req, res, next) => {
    if (req.session.sid != null) {
        next();
    }
    else {
        res.redirect("/");
    }
});

app.all('/data/*', (req, res, next) => {
    if (req.session.sid != null) {
        next();
    }
    else {
        res.redirect("/");
    }
});

// Serve static files via express
app.use(express.static(__dirname + '/client'));

// Use bcrypt for hashing passwords
const bcrypt = require('bcrypt');
const saltRounds = 10;

/* 
bcrypt.hash("testPassword", saltRounds, function(error, hash) {
    console.log(hash);
});
*/

// Connect to database
let database = new sqlite3.Database('./db/formData.db', (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Connected to disk-based main database file.");
})

// GET & POST begin

app.post('/login', (req, res) => {
    if (req.session.sid != null) {
        res.jsonp({ auth: true, action: "message", data: "You have already logged in!" }); 
    } else {
        // user and password from the client's request
        let user = req.body.user;
        let password = req.body.pass;

        // check that user is in database
        let sql = "SELECT * FROM User WHERE username=$user";
        let params = {
            $user : user
        };
        database.all(sql, params, (err, rows) => {
            if (err) {
                console.log(err);
            }

            if (rows.length > 0) { // user found
                bcrypt.compare(password, rows[0].password, function (hash_err, hash_resp) { // users are unique
                    if (hash_resp) { // password correct
                        req.session.sid = user;
                        res.jsonp({
                            auth: true,
                            nextStep: "goToAdminPanel",
                            additionalData: null
                        });

                        console.log("Successful login from IP %s: %s", req.ip, JSON.stringify(req.session));
                    } else { // password incorrect
                        res.jsonp({
                            auth: false,
                            nextStep: "invalidPassword",
                            additionalData: "Invalid password."
                        });
                        console.log("Failed login from IP %s, invalid password.", req.ip);
                    }
                });
            } else { // user not found
                res.jsonp({
                    auth: false,
                    nextStep: "invalidUser",
                    additionalData: "Invalid user."
                });
                console.log("Failed login from IP %s, invalid user.", req.ip);
            }
        });
    }
});

app.get('/logOut', (req, res) => {
    let sessid = req.session.sid;
    if (req.session.sid != null) {
        req.session.destroy((err) => {
            if (err) {
                console.log("\x1b[31m%s\x1b[0m", "[Logout] Error:\t" + err);
                res.jsonp({
                    auth: true,
                    nextStep: "error",
                    additionalData: "Error: " + err.message
                });
            }
            else {
                res.jsonp({
                    auth: true,
                    nextStep: "goToFrontPage",
                    additionalData: "Kirjauduttiin ulos onnistuneesti"
                });
                console.log("[Logout] User %s logged out.", sessid);
            }
        });
    }
    else {
        res.jsonp({
            auth: false,
            nextStep: "notLoggedIn",
            additionalData: "Et ole kirjautuneena sisään"
        });
    }
});

app.get('/form', (req,res) => {
    let formID = req.query.formID;
    processFormStart(formID, res);
});

app.post('/form/submit', (req, res) => {
    let name = req.body.name;
    let formID = req.body.formID;

    let mentalDemand = req.body.mentalDemand;
    let physicalDemand = req.body.physicalDemand;
    let temporalDemand = req.body.temporalDemand;
    let performance = req.body.performance;
    let effort = req.body.effort;
    let frustration = req.body.frustration;

    processFormSubmit(name, formID, mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration, res);
});

// Admin only, get CSV
app.get('/data/process', (req, res) => {
    if (req.session.sid != null) {
        let formID = req.query.id;
        processData(formID, res);
    }
});

// Admin only! Get all of the data from the form table, as well as the amount of responses (responseAmount) to the form thus far.
app.get('/forms', (req, res) => {
    if (req.session.sid != null) {
        database.all("SELECT Form.id, Form.name, Form.task, Form.expireDate, COUNT(FormData.formID) AS responseAmount FROM Form LEFT JOIN FormData ON FormData.formID = Form.id GROUP BY Form.id;", (err, rows) => {
            if (err) {
                console.log("Failed to retrieve the forms for the admin panel.");
                res.jsonp({
                    auth: true,
                    nextStep: "error",
                    additionalData: "Error: " + err.message
                });
            } else if (rows !== null && rows.length > 0) {
                console.log("Retrieved the forms for the admin panel.");
                res.jsonp({
                    auth: true,
                    nextStep: "listForms",
                    additionalData: rows
                });
            } 
        });
    } else {
        res.jsonp({
            auth: false,
            nextStep: "notLoggedIn",
            additionalData: "Et ole kirjautuneena sisään"
        });
        console.log("Failed to get forms for the admin panel, not logged in.");
    }
});

// Admin only! Add a new form/questionnaire. Request should contain name, task and expireDate.
app.post('/form/add', (req, res) => {
    if (req.session.sid != null) {
        let name = req.body.name;
        let task = req.body.task;
        let expireDate = req.body.expireDate;
        processFormAdd(name, task, expireDate, res);
    } else {
        res.jsonp({
            actionValid: false,
            nextStep: "notLoggedIn",
            additionalData: "Et ole kirjautuneena sisään"
        });
        console.log("Failed to add a new form, not logged in.");
    }
});

// Admin only! Modify form
app.post('/form/modify', (req, res) => {
    if (req.session.sid != null) {
        let id = req.body.id;
        let name = req.body.name;
        let task = req.body.task;
        let expireDate = req.body.expireDate;
        console.log(req.body);
        processFormModify(name, task, expireDate, id, res);
    } else {
        res.jsonp({
            actionValid: false,
            nextStep: "notLoggedIn",
            additionalData: "Et ole kirjautuneena sisään"
        });
        console.log("Failed to modify form, not logged in.");
    }
});

// Admin only! Remove form
app.post('/form/remove', (req, res) => {
    if (req.session.sid != null) {
        let id = req.body.id;
        processFormRemove(id, res);
    }
    else {
        res.jsonp({
            actionValid: false,
            nextStep: "notLoggedIn",
            additionalData: "Et ole kirjautuneena sisään"
        });
    }    
});

// Admin only! Get account data
app.get('/admin/getAccount', (req, res) => {
    if (req.session.sid != null) {
        res.jsonp({
            actionValid: true,
            nextStep: "accountData",
            additionalData : req.session.sid
        });
    }
    else {
        res.jsonp({
            actionValid: false,
            nextStep: "notLoggedIn",
            additionalData : null
        });
    }
});

// Admin only! Change password
app.post('/admin/changePass', (req, res) => {
    if (req.session.sid != null) {
        let pass = req.body.pass;
        let passAgain = req.body.passAgain;
        let minimumPassLength = 6;
    
        if (pass === passAgain && pass.length >= minimumPassLength) {
            bcrypt.hash(pass, saltRounds, function(error, hash) {
                if (error) {
                    res.jsonp({
                        actionValid: false,
                        nextStep: "couldNotHashPassword",
                        additionalData: error
                    });
                } else {
                    database.run("UPDATE User SET password = $hashedPass WHERE username = $username",
                    {$hashedPass : hash, $username : req.session.sid}, (err) => {
                        if (err) {
                            res.jsonp({
                                actionValid: false,
                                nextStep: "couldNotChangePassword",
                                additionalData: err.message
                            });
                        }
                        else {
                            res.jsonp({
                                actionValid: true,
                                nextStep: "success",
                                additionalData: "The password has been changed successfully."
                            });
                        }
                    });
                }
            });
        }
        else {
            res.jsonp({
                actionValid: false,
                nextStep: "passwordNotAcceptable",
                additionalData: { isLongEnough : pass.length >= minimumPassLength , isSame : pass === passAgain }
            });
        }
    }
    else {
        res.jsonp({
            actionValid: false,
            nextStep: "notLoggedIn",
            additionalData : null
        });
    }
});

// GET & POST end

// async functions start
// process xlsx
async function processData(formID, res_callback) {
    let success = null;
    await fetchDataFromDataBase(formID).then(ok => {
        console.log("Data fetched from db: %s", ok);
        success = ok;
    }).catch(err => {
        console.error("Error fetching data from db: %s", err);
    });

    if (success != null && success.length > 0) {
        let arr = [];
        await createAOA(success, success.length, 10).then(ok => {
            arr = ok;
        }).catch(err => {
            return console.error(err);
        });
        let ws = xlsx.utils.aoa_to_sheet(arr);
        //let wsName = "Results - " + formID;
        //let wb = xlsx.utils.book_new();

        //xlsx.utils.book_append_sheet(wb, ws, wsName);

        let filename = moment().format('DD-MM-YYYY_HH.mm.ss') + "_" + formID + ".csv";
        let fullPath = "./xlsx/" + filename;
        let csv = String.prototype.concat("sep=,\r\n", xlsx.utils.sheet_to_csv(ws));
        let i = 3;
        let saveOk = false;
        while (!saveOk && i-- >= 0) {
            try {
                if (await fs.existsSync(fullPath)) {
                    // ready
                    saveOk = true;
                    break;
                }
                else {
                    await fs.writeFileSync(fullPath, csv);
                    saveOk = true;
                    break;
                }
            }
            catch (err) {
                console.error("err: " + err.message);
                console.log("Trying again, " + i + " attempt(s) remaining.");
                saveOk = false;
            }
        }
        
        if (saveOk) {
            res_callback.download(fullPath, filename, function(err) {
                if (err) {
                    console.error("Download failure");
                }
                else {
                    console.log("Download complete");
                    deleteFileIfExists(fullPath).then(ok => {
                        console.log("delete promise: " + ok);
                    }).catch(err => {
                        console.error("delete promise err: " + err);
                    });
                }
            });
        }
        else {
            console.error("error streaming file to client");
            res_callback.jsonp({
                actionValid : false,
                nextStep : "serverError",
                additionalData: "Error streaming file to client"
            });
        }
        //res_callback.download(__dirname + "/xlsx/" + filename);

    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "invalidFormId",
            additionalData : null
        });
    }
}

// delete file
function deleteFileIfExists(file) {
    return new Promise((resolve, reject) => {
        console.log("delete file start");
        if (fs.existsSync(file)) {
            fs.unlinkSync(file, (err) => {
                if (err) {
                    console.error("err file delete: " + err.message);
                    reject(err);
                }
                else {
                    console.log("file deleted");
                    resolve(true);
                }
            });
        }
        else {
            console.log("file doesn't exist");
            resolve(false);
        }
    });
}

// create array of arrays (AOA)
function createAOA(data, rowsAmount, colsAmount) {
    return new Promise((resolve, reject) => {
        try {
            let rows = new Array(rowsAmount); // all rows
            let cols = []; // columns (names such as id, name, etc.)
            let tempRows = []; // temporary array for rows
            let index = 1; // current number of element
            let arri = 0; // row index
            data.map(function (el) {
                for (let key in el) {
                    if (cols.indexOf(key) === -1) cols.push(key); // add column if not present
                    
                    if (index++ % colsAmount === 0) { // every nth item: begin new row
                        tempRows.push(el[key]);
                        rows[arri++] = tempRows;
                        tempRows = [];
                        
                    }
                    else {
                        tempRows.push(el[key]);
                    }
                }
            });

            // correct aoa structure
            let final = [cols];
            for (let i = 0 ; i < rows.length; ++i) {
                final.push(rows[i]);
            }

            // log & resolve
            //console.log(final);
            resolve(final);
        }
        catch (err) {
            reject(err);
        }
    });
}

function fetchDataFromDataBase(formID) {
    return new Promise((resolve, reject) => {
        database.all("SELECT * FROM FormData WHERE formID=$formID", { $formID : formID }, (err, rows) => {
            if (err) {
                reject(err.message);
            }
            else if (rows !== null && rows.length > 0) {
                //console.log("data found");
                resolve(rows);
            }
            else {
                resolve(null);
            }
        });
    });
}
// process form start
async function processFormStart(id, res_callback) {
    let isValid = null;
    let task = null;
    let expireDate = null;
    let isExpired = true;

    await checkForValidFormId(id).then(formCheck => {
        isValid = formCheck["count"];
        task = formCheck["task"];
        expireDate = formCheck["expireDate"];
    }).catch(err => {
        isValid = null;
        console.log(err);
    });

    if (expireDate !== null) {
        await checkFormExpiration(expireDate.replace("_"," ")).then(ok => {
            isExpired = ok;
        }).catch(err => {
            isExpired = true;
            console.log(err);
        });
    }
    
    if (isValid !== null && isValid > 0 && !isExpired) {
        res_callback.jsonp({
            actionValid : true,
            nextStep : "goToForm",
            additionalData : { id : id, task: task }
        });
    }
    else if (isValid !== null && isValid > 0 && isExpired) {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "formExpired",
            additionalData : null
        });
    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "wrongFormId",
            additionalData : null
        });
    }
}

// function: check form expiration
function checkFormExpiration(rawDate) {
    return new Promise((resolve, reject) => {
        try {
            let parsedDate = moment(rawDate).toDate().getTime();
            let currentDate = moment().toDate().getTime();

            if (parsedDate < currentDate) {
                resolve(true);
            }
            else {
                resolve(false);
            }
        }
        catch (parseError) {
            reject(parseError.err);
        }
    });
}

// check form id
function checkForValidFormId(id) {
    return new Promise((resolve, reject) => {
        database.all("SELECT COUNT(*), task, expireDate FROM Form WHERE id=$id", {$id:id}, function (err, rows) {
            if (err) {
                reject(err.message);
            }
            else {
                resolve({count: rows[0]["COUNT(*)"], task: rows[0]["task"], expireDate: rows[0]["expireDate"]}); // also returns the task to be displayed in the form
            }
        });
    });
}

// process form submission
async function processFormSubmit(name, formID, mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration, res_callback) {
    let success = false;
    await saveFormDataToDataBase(name, formID, mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration).then(ok => {
        console.log("Form saving succeeded: %s", ok);
        success = true;
    }).catch(err => {
        console.error("Error saving form: %s", err);
    });
    
    if (success) {
        res_callback.jsonp({
            actionValid : true,
            nextStep : "submissionSuccess",
            additionalData : null
        });
    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "errorProcessingSubmission",
            additionalData : null
        });
    }
}

// process adding new form
async function processFormAdd(name, task, expireDate, res_callback) {
    let success = false;
    await saveFormToDataBase(name, task, expireDate).then(ok => {
        console.log("Form adding succeeded: %s", ok);
        success = true;
    }).catch(err => {
        console.error("Error adding form: %s", err);
    });
    
    if (success) {
        res_callback.jsonp({
            actionValid : true,
            nextStep : "formAddSuccess",
            additionalData : null
        });
    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "errorProcessingNewForm",
            additionalData : null
        });
    }
}

// process modifying form
async function processFormModify(name, task, expireDate, id, res_callback) {
    let success = false;
    await modifyFormToDataBase(id, name, task, expireDate).then(ok => {
        console.log("Modifying form succeeded: %s", ok);
        success = true;
    }).catch(err => {
        console.error("Error modifying form: %s", err);
    });
    
    if (success) {
        res_callback.jsonp({
            actionValid : true,
            nextStep : "formModifySuccess",
            additionalData : null
        });
    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "errorProcessingModifications",
            additionalData : null
        });
    }
}

// process deleting form
async function processFormRemove(id, res_callback) {
    let success = false;
    await removeFormFromDataBase(id).then(ok => {
        console.log("Removing form succeeded: %s", ok);
        success = true;
    }).catch(err => {
        console.error("Error removing form: %s", err);
    });
    
    if (success) {
        res_callback.jsonp({
            actionValid : true,
            nextStep : "formRemovalSuccess",
            additionalData : null
        });
    }
    else {
        res_callback.jsonp({
            actionValid : false,
            nextStep : "errorProcessingRemoval",
            additionalData : null
        });
    }
}

// save form submission data to database
function saveFormDataToDataBase(name, formID, mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration) {
    return new Promise((resolve, reject) => {
        database.run("INSERT INTO FormData(name, date, formID, mentalDemand, physicalDemand, temporalDemand, performance, effort, frustration) VALUES($name, $date, $formID, $mentalDemand, $physicalDemand, $temporalDemand, $performance, $effort, $frustration);",
        {$name : name, $date : moment().toISOString(), $formID : formID, $mentalDemand : mentalDemand, $physicalDemand : physicalDemand, $temporalDemand : temporalDemand, $performance : performance, $effort : effort, $frustration : frustration}, (err) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve(true);
            }
        });
    });
}

// save new form to database
function saveFormToDataBase(name, task, expireDate) {
    return new Promise((resolve, reject) => {
        database.run("INSERT INTO Form(name, task, expireDate) VALUES($name, $task, $expireDate);",
        {$name : name, $task : task, $expireDate : expireDate}, (err) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve(true);
            }
        });
    });
}

// save modified form to database
function modifyFormToDataBase(id, name, task, expireDate) {
    return new Promise((resolve, reject) => {
        database.run("UPDATE Form SET name=$name, task=$task, expireDate=$expireDate WHERE id=$id;",
        {$name : name, $task : task, $expireDate : expireDate, $id : id}, (err) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve(true);
            }
        });
    });
}

// delete form from database
function removeFormFromDataBase(id) {
    return new Promise((resolve, reject) => {
        database.run("DELETE FROM Form WHERE id=$id;",
        {$id : id}, (err) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve(true);
            }
        });
    });
}

// async functions end

// Listen on port defined by variable "port" (default 8080)
app.listen(port, '0.0.0.0', () => console.log(`TLX Form Server is now running on ${port}.`));