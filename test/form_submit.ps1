﻿# Nasa TLX Web Form
# Submit test results
# Powershell Script, 26.3.2019
$Url = "http://localhost:8080/form/submit"
$Body = @{
    name = "testi"
    formID = "1"
    mentalDemand = "1"
    physicalDemand = "3"
    temporalDemand = "3"
    performance = "7"
    effort = "6"
    frustration = "9"
}

Invoke-RestMethod -Method 'Post' -Uri $url -Body $Body 