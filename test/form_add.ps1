# Nasa TLX Web Form
# Add new form (admin only, running this script should not result in anything being added to the database under normal circumstances)
# Powershell Script, 8.4.2019
$Url = "http://localhost:8080/form/add"
$Body = @{
    name = "testi"
    task = '{"en":"TestTask","fi":"TestiTehtävä"}'
    expireDate = "2019-04-08"
}

Invoke-RestMethod -Method 'Post' -Uri $url -Body $Body 