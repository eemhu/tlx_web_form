# NASA-TLX Web Form

Nasa-TLX Form for web browsers.

## Server setup

Node.js and node package manager (npm) is required to complete the installation. To start the server, please run start.bat (if using a Windows based system) or start.sh (for a Linux based system). The startup script installs the necessary modules during the first startup. Script also asks for the port, in which the server will run. Default port is 8080.

You can also start the server manually if you so desire:
```
$ npm install
$ node server.js [-p PORT]
```

The following output should appear once the server is running:
```
TLX Form Server is now running on <CUSTOM PORT or default 8080>.
Connected to disk-based main database file.
```
Please login to the web application at http://localhost:PORT once the application is running. The default credentials are "admin" and "testPassword". You can (and should) change the password from the admin panel, after logging in. After setting up the server and the admin credentials, you can manage forms and download responses using the web interface.

## Form management (adding, editing and removing forms)
You can manage forms in the administrator view (http://localhost:PORT/admin) after logging in. Expired forms can no longer be responded to. Please note that the expiration time is checked using the server's time. If the server computer's time is past the expiration time, the form can no longer be accessed. You can, however, download the responses to the form from the admin panel, or give a later expiration time for the form in order to allow for more responses.

## Downloading responses
To download the responses to a form that you have created, find it in the list in the admin view and find the "Download .csv" -link. If there are no responses to download, the link will not be visible. The amount of responses is also visible in the list.

## Troubleshooting

### Lost/forgotten password
If necessary, you can reset the password on the server computer using the following method.
1. Install DB Browser for SQLite or a similiar program for editing the database
2. Open db/formData.db with the database browser
3. In the User table, insert the following hash value in the password field. This is the hash value for "testPassword", the default password.
```
$2b$10$.3TGy2vw9ZwgLluLqlG8YOux8vXIxR3qS49loRgSRl.k08cmqrni.
```
4. Save/write the changes to the database.
5. You should be able to login using the default password "testPassword".

By default there is only one admin account in the system.

### Issues with downloading responses
- Ensure that a folder called xlsx exists within this folder. However, the folder in question should be created automatically.

### Other/general
- Restarting the application may help in some cases