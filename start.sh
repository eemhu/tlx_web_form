#!/bin/bash

# NASA-TLX web form startup script for Linux
echo '-- NASA TLX Web form startup script'
echo '-- version 2019-04-29'
echo '-------------------------'

if [ $(id -u) = 0 ]; then
	echo '--Script is not running as root, if you experience any issues, please run script as root user'
fi

echo "--Enter port where the server will run (default 8080): "
read port

if [ -f 'server.js' ]; then
	echo '--Server file ok'

	if [ -d 'node_modules/' ]; then
		echo '--Server modules ok, checking updates'
		npm install
		echo '--Starting server'
		node server.js -p $port
		exit
	else
		echo '--Server modules missing, downloading them now'
		npm install
		echo '--Starting server'
		node server.js -p $port
		exit
	fi
else
	echo '--Server files are missing, please run this script at server files root folder'
	exit
fi
